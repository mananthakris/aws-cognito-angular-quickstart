export const environment = {
    production: false,

    region: 'us-east-1',

    identityPoolId: 'us-east-1:fbe0340f-9ffc-4449-a935-bb6a6661fd53',
    userPoolId: 'us-east-1_Sl3c0YdIZ',
    clientId: '5von1img2udlqhuekb7atfk1b3',

    rekognitionBucket: 'rekognition-pics',
    albumName: "usercontent",
    bucketRegion: 'us-east-1',

    ddbTableName: 'LoginTrail',

    cognito_idp_endpoint: '',
    cognito_identity_endpoint: '',
    sts_endpoint: '',
    dynamodb_endpoint: '',
    s3_endpoint: ''

};

